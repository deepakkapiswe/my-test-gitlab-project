module Lib
    where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

type Title = String

data Grid = GridConf {
            size::Size
           ,robotPos::Pos
           } deriving (Eq)




instance Show Grid where
 show g = "OUR GRID IS \n" ++ (makeBox$size g)

data RoboCommand = L|U|R|D deriving (Show,Eq)

type Size = (Int,Int)
type Pos = (Int,Int)


changeRoboPos'::Grid->Title->RoboCommand->(Grid,Title)
changeRoboPos' g t com = let newPos = getNewPos g com
                       in  case (isOutSide g newPos) of 
                            False ->(g {robotPos = newPos},t)
                            _ -> (g,t)

changeRoboPos''::Grid->RoboCommand->Grid
changeRoboPos'' g com = case (isOutSide g newPos) of 
                            False -> g {robotPos = newPos}
                            _ -> g
 where
  newPos = getNewPos g com

isOutSide::Grid->Pos->Bool
isOutSide (GridConf (x,y) _) (x',y') = (x'>x) || (y'>y)

getNewPos::Grid->RoboCommand->Pos
getNewPos (GridConf _ (x,y)) com = case com of 
 L -> (x-1,y)
 U -> (x,y+1)
 R -> (x+1,y)
 D -> (x,y-1)

applyNtimes::Grid->Title->RoboCommand->Int->(Grid,Title)
applyNtimes = undefined


f::Int->Int
f c = c
g = GridConf (10,10) (5,5)

--dsfljsfjsfdlkj
--dsfljsfjsfdlkj
--dsfljsfjsfdlkj


makeBox::(Int,Int)->String
makeBox (row,col) = unlines $replicate row $ replicate col '#'
